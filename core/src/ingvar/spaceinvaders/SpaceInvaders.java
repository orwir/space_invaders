package ingvar.spaceinvaders;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import ingvar.spaceinvaders.screens.MenuScreen;

public class SpaceInvaders extends Game {

    public static final boolean DEBUG = false;

    private Skin skin;

    public Skin getSkin() {
        return skin;
    }

    @Override
    public void create() {
        skin = new Skin(Gdx.files.internal("skin-default/uiskin.json"), new TextureAtlas("skin-default/uiskin.atlas"));
        setScreen(new MenuScreen(this));
    }

    @Override
    public void dispose() {
        super.dispose();
        skin.dispose();
    }
}
