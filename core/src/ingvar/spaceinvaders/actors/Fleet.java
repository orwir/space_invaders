package ingvar.spaceinvaders.actors;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import ingvar.spaceinvaders.Textures;
import ingvar.spaceinvaders.screens.GameScreen;

import java.util.LinkedList;
import java.util.List;

public class Fleet extends WidgetGroup {

    private static final float SPEED = .3f;
    private static final float MOVE_Y = -3;
    private static final float BULLET_DELAY = 1.6f;

    private GameScreen gameScreen;
    private TextureAtlas textures;
    private TextureRegion bulletView;
    private List<Invader> invaders;
    private float direction;

    public Fleet(TextureAtlas textures, GameScreen screen) {
        this.textures = textures;
        this.bulletView = textures.findRegion(Textures.BULLET_ENEMY);
        this.gameScreen = screen;
        initFleet();

        addAction(Actions.forever(Actions.delay(BULLET_DELAY, new Action() {
            @Override
            public boolean act(float delta) {
                if(invaders.size() > 0) {
                    int shooter = MathUtils.random(0, invaders.size() - 1);
                    Invader invader = invaders.get(shooter);
                    Projectile bullet = gameScreen.PROJECTILES.obtain();
                    Vector2 pos = new Vector2();
                    invader.localToStageCoordinates(pos);
                    bullet.init(invader, bulletView, pos.x + invader.getWidth() / 2, pos.y - 1, -GameScreen.BULLET_SPEED);
                    gameScreen.fire(bullet);
                }
                return true;
            }
        })));
    }

    public void restart() {
        for(Invader invader : invaders) {
            removeActor(invader);
        }
        initFleet();
    }

    public List<Invader> getInvaders() {
        return invaders;
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        float moveX = SPEED * direction;
        float moveY = 0;
        if(getX() < 0) {
            moveX = -getX();
            direction *= -1;
            moveY = MOVE_Y;
        }
        if(getX() + getWidth() > getStage().getWidth()) {
            moveX = getStage().getWidth() - (getX() + getWidth());
            direction *= -1;
            moveY = MOVE_Y;
        }
        moveBy(moveX, moveY);
        if(invaders.size() == 0) {
            gameScreen.playerWin();
        }
        if(getY() < 0) {
            gameScreen.invadersWin();
        }
    }

    private void initFleet() {
        direction = 1;

        float height = 0;
        float width  = 0;
        invaders = new LinkedList<Invader>();
        for(int col = 0; col < 1; col++) {
            TextureRegion enemy = textures.findRegion(Textures.ENEMY_ + Integer.toString(col));
            height += enemy.getRegionHeight() + enemy.getRegionHeight() / 2;
            for(int row = 0; row < 8; row++) {
                Invader invader = new Invader(enemy);
                invaders.add(invader);
                addActor(invader);
                float posX = row * (invader.getX() + invader.getWidth()) + ((invader.getWidth() / 2) * row);
                invader.setPosition(posX, col);
                width = posX + invader.getWidth();
            }
        }
        setSize(width, height);
    }

}
