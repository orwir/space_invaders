package ingvar.spaceinvaders.actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import ingvar.spaceinvaders.Textures;
import ingvar.spaceinvaders.screens.GameScreen;

public class Player extends Fighter {

    public static final int FIRE = 1;
    public static final int MOVE_LEFT = 2;
    public static final int MOVE_RIGHT = 4;
    private static final float BULLET_DELAY = .5f;

    private GameScreen gameScreen;
    private TextureRegion playerView;
    private TextureRegion bulletView;
    private int actions;
    private float time;

    public Player(TextureAtlas atlas, GameScreen screen) {
        super(Type.DEFENDER);
        this.gameScreen = screen;
        this.playerView = atlas.findRegion(Textures.PLAYER);
        this.bulletView = atlas.findRegion(Textures.BULLET_PLAYER);
        setSize(playerView.getRegionWidth(), playerView.getRegionHeight());
        time = 0;
    }

    public void updateActions(int action) {
        actions += action;
        if(actions < 0) {
            actions = 0;
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(playerView, getX(), getY(), getWidth(), getHeight());
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        float move = 0;
        if((actions & MOVE_LEFT) == MOVE_LEFT && (actions & MOVE_RIGHT) == MOVE_RIGHT) {
            //do nothing
        }
        else if((actions & MOVE_LEFT) == MOVE_LEFT) {
            move = -5;
            if(getX() + move < 0) {
                move = -getX();
            }
        }
        else if((actions & MOVE_RIGHT) == MOVE_RIGHT) {
            move = 5;
            if(getX() + getWidth() + move > getStage().getWidth()) {
                move = getStage().getWidth() - (getX() + getWidth());
            }
        }
        if(move != 0) {
            moveBy(move, 0);
        }

        if((actions & FIRE) == FIRE) {
            if(time >= BULLET_DELAY || time == 0) {
                Projectile bullet = gameScreen.PROJECTILES.obtain();
                bullet.init(this, bulletView, getCenterX(), getY() + getHeight() + 1, GameScreen.BULLET_SPEED);
                gameScreen.fire(bullet);
                time = 0;
            }
        }
        time += delta;
    }



}
