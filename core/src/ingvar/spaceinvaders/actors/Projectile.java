package ingvar.spaceinvaders.actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import ingvar.spaceinvaders.screens.GameScreen;

public class Projectile extends Actor {

    private GameScreen gameScreen;

    private Fighter owner;
    private TextureRegion view;
    private float moveY;

    public Projectile(GameScreen gameScreen) {
        this.gameScreen = gameScreen;
    }

    public void init(Fighter owner, TextureRegion view, float posX, float posY, float moveY) {
        this.owner = owner;
        this.view  = view;
        this.moveY = moveY;
        setSize(view.getRegionWidth(), view.getRegionHeight());
        setPosition(posX, posY);
    }

    @Override
    public void act(float delta) {
        boolean isHit = false;
        if(Fighter.Type.DEFENDER.equals(owner.getType())) {
            for(Invader invader : gameScreen.getInvaders()) {
                Vector2 coord = new Vector2();
                invader.localToStageCoordinates(coord);
                Rectangle rect = new Rectangle(coord.x, coord.y, invader.getWidth(), invader.getHeight());
                if(rect.contains(getCenterX(), getCenterY())) {
                    gameScreen.removeProjectile(this);
                    gameScreen.hitInvader(invader);
                    isHit = true;
                    break;
                }
            }
        }
        else if(Fighter.Type.INVADER.equals(owner.getType())) {
            Actor defender = gameScreen.getPlayer();
            Rectangle rect = new Rectangle(defender.getX(), defender.getY(), defender.getWidth(), defender.getHeight());
            if(rect.contains(getCenterX(), getCenterY())) {
                gameScreen.removeProjectile(this);
                gameScreen.hitPlayer();
                isHit = true;
            }
        }
        if(!isHit) {
            if (getCenterY() < 0 || getCenterY() > getStage().getHeight()) {
                getStage().getRoot().removeActor(this);
                gameScreen.PROJECTILES.free(this);
            } else {
                moveBy(0, moveY);
            }
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(view, getX(), getY(), getWidth(), getHeight());
    }
}
