package ingvar.spaceinvaders.actors;

import com.badlogic.gdx.scenes.scene2d.Actor;

public abstract class Fighter extends Actor {

    public static enum Type {
        DEFENDER,
        INVADER;
    }

    private Type type;

    public Fighter(Type type) {
        this.type = type;
    }

    public Type getType() {
        return type;
    }
}
