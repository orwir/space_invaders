package ingvar.spaceinvaders.actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Invader extends Fighter {

    private TextureRegion invaderView;

    public Invader(TextureRegion view) {
        super(Type.INVADER);
        this.invaderView = view;
        setSize(invaderView.getRegionWidth(), invaderView.getRegionHeight());
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(invaderView, getX(), getY(), getWidth(), getHeight());
    }

}
