package ingvar.spaceinvaders.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import ingvar.spaceinvaders.SpaceInvaders;

public class HelpScreen implements Screen {

    private Stage hud;

    public HelpScreen(final SpaceInvaders game) {
        hud = new Stage(new ScreenViewport());

        Skin skin = game.getSkin();

        Table table = new Table(skin);
        hud.addActor(table);
        table.setFillParent(true);
        table.add(new Label("Description", skin)).padBottom(16).row();
        table.add(new Label("Destroy all invaders before they invade your planet.\nAnd try to evade their projectiles", skin)).padBottom(32).row();
        table.add(new Label("Controls", skin)).padBottom(16).row();
        table.add(new Label("LEFT/RIGHT - move\nSPACE - fire", skin)).padBottom(16).row();

        Button btnBack = new TextButton("Back", skin);
        table.add(btnBack);

        btnBack.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new MenuScreen(game));
            }
        });
    }

    @Override
    public void render(float delta) {
        GL20 gl = Gdx.gl;
        gl.glClearColor(0, 0, 0, 1);
        gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        hud.act(delta);
        hud.draw();
        if(SpaceInvaders.DEBUG) {
            Table.drawDebug(hud);
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(hud);
    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        hud.dispose();
    }
}
