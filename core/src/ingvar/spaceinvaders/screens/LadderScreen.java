package ingvar.spaceinvaders.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import ingvar.spaceinvaders.SpaceInvaders;

import java.util.Map;

public class LadderScreen implements Screen {

    private Stage hud;

    public LadderScreen(final SpaceInvaders game) {
        hud = new Stage(new ScreenViewport());

        Skin skin = game.getSkin();
        Table table = new Table(skin);
        hud.addActor(table);
        table.setFillParent(true);
        table.top();

        table.add(new Label("Hall Of Fame", skin)).padBottom(16).row();

        List<Leader> list = new List<Leader>(skin);
        list.setItems(new Array<Leader>());
        Preferences prefs = Gdx.app.getPreferences(SpaceInvaders.class.getSimpleName());
        Map<String, String> leaders = (Map<String, String>) prefs.get();
        for(Map.Entry<String, String> leader : leaders.entrySet()) {
            list.getItems().add(new Leader(leader.getKey(), Integer.valueOf(leader.getValue())));
        }
        list.getItems().sort();

        table.add(list).padBottom(16).expandY().row();

        Button btnBack = new TextButton("Back", skin);
        table.add(btnBack).padBottom(16);

        btnBack.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new MenuScreen(game));
            }
        });
    }

    @Override
    public void render(float delta) {
        GL20 gl = Gdx.gl;
        gl.glClearColor(0, 0, 0, 1);
        gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        hud.act(delta);
        hud.draw();
        if(SpaceInvaders.DEBUG) {
            Table.drawDebug(hud);
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(hud);
    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        hud.dispose();
    }

    private class Leader implements Comparable<Leader> {

        String name;
        Integer score;

        public Leader(String name, Integer score) {
            this.name = name;
            this.score = score;
        }

        @Override
        public int compareTo(Leader o) {
            return (score == null || o == null) ? 0 : -score.compareTo(o.score);
        }

        @Override
        public String toString() {
            return name + " : " + score;
        }

    }

}
