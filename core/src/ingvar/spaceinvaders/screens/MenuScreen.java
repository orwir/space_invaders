package ingvar.spaceinvaders.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import ingvar.spaceinvaders.SpaceInvaders;

public class MenuScreen implements Screen {

    private Stage hud;

    public MenuScreen(final SpaceInvaders game) {
        hud = new Stage(new ScreenViewport());
        Skin skin = game.getSkin();

        Label lblHeader = new Label("Space Invaders", skin);
        Button btnPlay = new TextButton("Play game", skin);
        btnPlay.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new GameScreen(game));
            }
        });
        Button btnHelp = new TextButton("Help", skin);
        btnHelp.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new HelpScreen(game));
            }
        });
        Button btnLadder = new TextButton("Hall of Fame", skin);
        btnLadder.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new LadderScreen(game));
            }
        });

        Table table = new Table(skin);
        hud.addActor(table);
        table.setFillParent(true);
        if(SpaceInvaders.DEBUG) {
            table.debug();
        }

        table.add(lblHeader).padBottom(16).row();
        table.add(btnPlay).width(100).padBottom(16).row();
        table.add(btnHelp).width(100).padBottom(16).row();
        table.add(btnLadder).width(100);
    }

    @Override
    public void render(float delta) {
        GL20 gl = Gdx.gl;
        gl.glClearColor(0, 0, 0, 1);
        gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        hud.act(delta);
        hud.draw();
        if(SpaceInvaders.DEBUG) {
            Table.drawDebug(hud);
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(hud);
    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
        dispose();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        hud.dispose();
    }

}
