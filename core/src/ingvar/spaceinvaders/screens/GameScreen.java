package ingvar.spaceinvaders.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import ingvar.spaceinvaders.SpaceInvaders;
import ingvar.spaceinvaders.Textures;
import ingvar.spaceinvaders.actors.Fleet;
import ingvar.spaceinvaders.actors.Invader;
import ingvar.spaceinvaders.actors.Player;
import ingvar.spaceinvaders.actors.Projectile;

import java.util.ArrayList;
import java.util.List;

public class GameScreen implements Screen {

    public final Pool<Projectile> PROJECTILES = new Pool<Projectile>() {
        @Override
        protected Projectile newObject() {
            return new Projectile(GameScreen.this);
        }
    };
    public static final float BULLET_SPEED = 5;

    private Stage stage;

    private TextureAtlas textures;

    private Label lblScore;
    private Label lblScoreValue;
    private Label lblLives;
    private Image imgLive1;
    private Image imgLive2;
    private Image imgLive3;
    private Label lblEnd;
    private Button btnRestart;
    private Button btnExit;
    private int lives;
    private int score;
    private boolean gameRunning;

    private Player actPlayer;
    private Fleet invaders;
    private List<Projectile> projectiles;

    public GameScreen(SpaceInvaders game) {
        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 600);
        stage = new Stage(new ScreenViewport(camera));
        textures = new TextureAtlas("textures.atlas");
        projectiles = new ArrayList<Projectile>();

        lives = 3;
        gameRunning = true;
        initHUD(game);
        initActors();
        initInput();
    }

    public void fire(Projectile projectile) {
        stage.addActor(projectile);
        projectiles.add(projectile);
    }

    public void removeProjectile(Projectile projectile) {
        stage.getRoot().removeActor(projectile);
        PROJECTILES.free(projectile);
    }

    public Player getPlayer() {
        return actPlayer;
    }

    public List<Invader> getInvaders() {
        return invaders.getInvaders();
    }

    public void hitPlayer() {
        switch (lives--) {
            case 3:
                imgLive3.setVisible(false);
                break;
            case 2:
                imgLive2.setVisible(false);
                break;
            case 1:
                imgLive1.setVisible(false);
                break;
            case 0:
                invadersWin();
                break;
        }
        if(lives >= 0) {
            score -= 10;
        }
    }

    public void hitInvader(Invader invader) {
        invaders.getInvaders().remove(invader);
        invaders.removeActor(invader);
        score += 10;
        lblScoreValue.setText(Integer.toString(score));
    }

    public void playerWin() {
        endGame("Invaders was defeated.\nCongratulations!");
    }

    public void invadersWin() {
        endGame("Invaders invade planet.\nYou lose!");
    }

    public void restart() {
        lives = 3;
        score = 0;
        gameRunning = true;
        imgLive1.setVisible(true);
        imgLive2.setVisible(true);
        imgLive3.setVisible(true);
        lblEnd.setVisible(false);
        btnRestart.setVisible(false);
        btnExit.setVisible(false);

        actPlayer.setPosition(stage.getWidth() / 2 - actPlayer.getWidth() / 2, 10);
        invaders.setPosition(stage.getWidth() / 2 - invaders.getWidth() / 2, stage.getHeight() - 50 - invaders.getHeight());
        invaders.restart();
        for(Projectile p : projectiles) {
            stage.getRoot().removeActor(p);
        }
        projectiles.clear();
    }

    @Override
    public void render(float delta) {
        GL20 gl = Gdx.gl;
        gl.glClearColor(0, 0, 0, 1);
        gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if(gameRunning) {
            stage.act(delta);
        }
        stage.draw();
        if(SpaceInvaders.DEBUG) {
            Table.drawDebug(stage);
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
        dispose();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        textures.dispose();
        stage.dispose();
    }

    private void initHUD(final SpaceInvaders game) {
        Skin skin = game.getSkin();

        lblScore = new Label("SCORE", skin);
        lblScoreValue = new Label("0", skin);
        lblLives = new Label("LIVES", skin);
        TextureRegion txrLive = textures.findRegion(Textures.PLAYER);
        imgLive1 = new Image(txrLive);
        imgLive2 = new Image(txrLive);
        imgLive3 = new Image(txrLive);
        lblEnd = new Label("", skin);
        btnRestart = new TextButton("Restart", skin);
        btnExit = new TextButton("Exit", skin);

        lblEnd.setVisible(false);
        btnRestart.setVisible(false);
        btnExit.setVisible(false);

        Table hud = new Table(skin);
        hud.setZIndex(10);
        stage.addActor(hud);
        hud.setFillParent(true);
        hud.top().left().padLeft(16).padRight(16).padTop(16);
        if(SpaceInvaders.DEBUG) {
            hud.debug();
        }

        hud.add(lblScore);
        hud.add(lblScoreValue).padLeft(16);
        hud.add(new Label("", skin)).expandX();
        hud.add(lblLives);
        hud.add(imgLive1).padLeft(16);
        hud.add(imgLive2).padLeft(16);
        hud.add(imgLive3).padLeft(16);
        hud.row();
        hud.add(new Label("", skin)).expandY().row();
        hud.add(lblEnd).colspan(7).padBottom(16).row();
        hud.add(btnRestart).colspan(7).padBottom(16).row();
        hud.add(btnExit).colspan(7).padBottom(16).row();
        hud.add(new Label("", skin)).expandY();

        btnRestart.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                restart();
            }
        });
        btnExit.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new MenuScreen(game));
            }
        });
    }

    private void initActors() {
        actPlayer = new Player(textures, this);
        stage.addActor(actPlayer);
        actPlayer.setPosition(stage.getWidth() / 2 - actPlayer.getWidth() / 2, 10);

        invaders = new Fleet(textures, this);
        stage.addActor(invaders);
        invaders.setPosition(stage.getWidth() / 2 - invaders.getWidth() / 2, stage.getHeight() - 50 - invaders.getHeight());
    }

    private void initInput() {
        stage.addListener(new ClickListener() {
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                boolean handled = true;
                switch (keycode) {
                    case Input.Keys.LEFT:
                        actPlayer.updateActions(Player.MOVE_LEFT);
                        break;
                    case Input.Keys.RIGHT:
                        actPlayer.updateActions(Player.MOVE_RIGHT);
                        break;
                    case Input.Keys.SPACE:
                        actPlayer.updateActions(Player.FIRE);
                        break;
                    default:
                        handled = false;
                        break;
                }
                return handled || super.keyDown(event, keycode);
            }

            @Override
            public boolean keyUp(InputEvent event, int keycode) {
                boolean handled = true;
                switch (keycode) {
                    case Input.Keys.LEFT:
                        actPlayer.updateActions(-Player.MOVE_LEFT);
                        break;
                    case Input.Keys.RIGHT:
                        actPlayer.updateActions(-Player.MOVE_RIGHT);
                        break;
                    case Input.Keys.SPACE:
                        actPlayer.updateActions(-Player.FIRE);
                        break;
                    default:
                        handled = false;
                        break;
                }
                return handled || super.keyUp(event, keycode);
            }
        });
    }

    private void endGame(String text) {
        lblEnd.setText(text);
        lblEnd.setVisible(true);
        btnRestart.setVisible(true);
        btnExit.setVisible(true);
        gameRunning = false;

        Gdx.input.getTextInput(new NameInputListener(), "Input your name", "Anon");
    }

    private class NameInputListener implements Input.TextInputListener {

        @Override
        public void input(String text) {
            Preferences prefs = Gdx.app.getPreferences(SpaceInvaders.class.getSimpleName());
            prefs.putString(text, Integer.toString(score));
            prefs.flush();
        }

        @Override
        public void canceled() {

        }
    }

}
