package ingvar.spaceinvaders;

public class Textures {

    public static final String PLAYER = "player";
    public static final String ENEMY_ = "enemy";
    public static final String BULLET_PLAYER = "bullet-player";
    public static final String BULLET_ENEMY = "bullet-enemy";

}
